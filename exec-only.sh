# Clones the repository containing the ansible code
git clone git@gitlab.com:tpsinghzck/terra-ansible.git

# gets into the repository directory
cd terra-ansible

# Enter the name of the terraform repository to execute. In Jenkins this will be passed as a parameter.
# url1=git@gitlab.com:tpsinghzck/terra-vpc.git
# url2=git@gitlab.com:tpsinghzck/s3-terra.git
echo "Enter the terraform repository"
read url

# Clones the terraform repository
git clone $url

# Gets the basename of the repository URL. Eg: For git@gitlab.com:tpsinghzck/s3-terra.git, it will be "s3-terra.git"
basename=$(basename $url)

# Filtering basename to get only the folder name. For "s3-terra.git", it will output "s3-terra"
filename=${basename%.*}

# Get into terraform project directory
cd $filename

#List file in bucket. In Jenkins we will have to pass the ID in paramater
aws s3 ls s3://"$filename-s3-bucket"

# Enter the build/time of the file is S3. Eg: If terra.tfplan-s3-terra-2020120715 is the file
# enter the 2020120715 part
read -p " Enter the time/build number for the plan file required: " id
echo $id

# Copy the ansible_vars.yml for the particular terraform project into different role folders.
cp ansible_vars.yml ../roles/terra-init/vars/main.yml 
cp ansible_vars.yml ../roles/terra-plan/vars/main.yml 
cp ansible_vars.yml ../roles/terra-exec/vars/main.yml

# varibale name to append at the start
varname="terra.tfplan"
#the complete file name
file=uri="-$filename-$id"


#copies the plan file from s3 to local system
aws s3 cp s3://"$filename-s3-bucket"/"$varname-$filename-$id" "$varname-$filename-$id"

# Navigate to the parent directory
cd ..

# Running the selected playbook and wait for the result
echo " Running the playbook will execute the plan file "
ansible-playbook exec.yaml --extra-vars "time=$id"