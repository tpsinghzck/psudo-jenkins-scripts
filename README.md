# psudo-jenkins-scripts

Scripts to mimic the behavior of CICD pipelines

These are three script which mimic the behaviour of CICD, the parameters that are being input will need to be passed as parameters in CICD tool such as Jenkins/Gitlab-CI

The three scripts are:

- do-everything.sh: Deploys everything from terraform init to terraform apply

- only-plan.sh: Will create a plan from .tfvars and send it to S3

- exec-only.sh: Will copy a plan from S3 that we specify and apply the changes.

Go to the following repo for running ansible: [https://gitlab.com/tpsinghzck/terra-ansible]